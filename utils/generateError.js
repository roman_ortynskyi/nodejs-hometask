const generateError = (message) => {
    return {
        error: true,
        message,
    };
}

exports.generateError = generateError;