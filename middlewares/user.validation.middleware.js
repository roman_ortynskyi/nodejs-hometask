const { generateError } = require('../utils/generateError');

const validateUser = (user) => {
    const {
        id,
        firstName,
        lastName,
        email,
        phoneNumber,
        password,
    } = user;

    const fields = [
        'firstName',
        'lastName',
        'email',
        'phoneNumber',
        'password',
    ];

    const keys = Object.keys(user);
    for(let key of keys) {
        if(!fields.includes(key)) {
            return generateError(`field ${key} is not allowed`);
        }
    }

    if(id !== undefined) {
        return generateError('id should be absent');
    }

    // required fields 
    if(firstName === undefined) {
        return generateError('firstName is required');
    } 

    if(lastName === undefined) {
        return generateError('lastName is required');
    }

    if(email === undefined) {
        return generateError('email is required');
    }

    if(phoneNumber === undefined) {
        return generateError('phoneNumber is required');
    }

    if(password === undefined) {
        return generateError('password is required');
    }

    // types
    if(typeof firstName !== 'string') {
        return generateError('firstName should be a string');
    }

    if(typeof lastName !== 'string') {
        return generateError('lastName should be a string');
    }

    if(typeof email !== 'string') {
        return generateError('email should be a string');
    }

    if(typeof password !== 'string') {
        return generateError('password should be a string');
    }

    if(typeof phoneNumber !== 'string') {
        return generateError('phoneNumber should be a string');
    }

    // format
    const gmailRegex = /^[\w.+\-]+@gmail\.com$/;
    if(!gmailRegex.test(email)) {
        return generateError('email is invalid');
    }

    const phoneNumberRegex = /^\+380\d{9}$/;
    if(!phoneNumberRegex.test(phoneNumber)) {
        return generateError('phoneNumber is invalid');
    }

    if(password.length < 3) {
        return generateError('password should contain at least 3 characters');
    }
}

const createUserValid = (req, res, next) => {
    req.validationError = validateUser(req.body);
    
    next();
}

const updateUserValid = (req, res, next) => {
    req.validationError = validateUser(req.body);

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;