const { generateError } = require('../utils/generateError');

const validateFighter = (fighter) => {
    const {
        id,
        name,
        health,
        power,
        defense,
    } = fighter;

    const fields = [
        'name',
        'health',
        'power',
        'defense',
    ];

    const keys = Object.keys(fighter);
    for(let key of keys) {
        if(!fields.includes(key)) {
            return generateError(`field ${key} is not allowed`);
        }
    }

    if(id !== undefined) {
        return generateError('id should be absent');
    }

    // required fields 
    if(name === undefined) {
        return generateError('name is required');
    } 

    if(power === undefined) {
        return generateError('power is required');
    } 

    if(defense === undefined) {
        return generateError('defense is required');
    } 

    // types
    if(typeof name !== 'string') {
        return generateError('name should be a string');
    }

    if(health !== undefined && typeof health !== 'number') {
        return generateError('health should be a number');
    }

    if(typeof power !== 'number') {
        return generateError('power should be a number');
    }

    if(typeof defense !== 'number') {
        return generateError('defense should be a number');
    }
}

const createFighterValid = (req, res, next) => {
    req.validationError = validateFighter(req.body);
    
    next();
}

const updateFighterValid = (req, res, next) => {
    req.validationError = validateFighter(req.body);

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;