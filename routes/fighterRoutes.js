const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { generateError } = require('../utils/generateError');

const router = Router();

// TODO: Implement route controllers for fighter
router.post('/', createFighterValid, (req, res, next) => {
    if(req.validationError) {
        next();
    }
    else {
        try {
            const createdFighter = FighterService.create(req.body);
            req.result = {
                status: 200,
                body: createdFighter,
            };
        }
        catch(e) {
            req.result = {
                status: 400,
                body: generateError(e.message),
            }
        }
        finally {
            next();
        }
    }
    
}, responseMiddleware);

router.get('/', (req, res, next) => {
    const fighters = FighterService.getAll();
    req.result = {
        status: 200,
        body: fighters,
    }
    next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    const { id } = req.params;  
    const fighter = FighterService.getOne({ id });
    req.result = fighter ? {
        status: 200,
        body: fighter,
    } :
    {
        status: 404,
        body: generateError(`Fighter ${id} does not exist`),
    };
    next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    const { id } = req.params;
    const updatedFighter = FighterService.update(id, req.body);
    req.result = {
        status: 200,
        body: updatedFighter,
    };
    next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    const { id } = req.params;
    FighterService.delete(id);
    req.result = {
        status: 200,
    }
    next();
}, responseMiddleware);

module.exports = router;