const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    create(fighter) {
        const { name } = fighter;
        const fighterWithName = FighterRepository.getOne({ name });
        if(fighterWithName) throw new Error('Fighter exists');

        const createdFighter = FighterRepository.create(fighter);
        if(!createdFighter) {
            return null;
        }
        return createdFighter;
    }

    getAll() {
        const fighters = FighterRepository.getAll();
        if(!fighters) {
            return [];
        }
        return fighters;
    }

    getOne({ id }) {
        const item = FighterRepository.getOne({ id });
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const fighter = FighterRepository.update(id, dataToUpdate);
        if(!fighter) {
            return null;
        }
        return fighter;
    }

    delete(id) {
        FighterRepository.delete(id);
    }
}

module.exports = new FighterService();